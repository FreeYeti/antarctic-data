# Antarctic data (deprecated)

See: [https://gitlab.com/FreeYeti/antarctic-geoserver](https://gitlab.com/FreeYeti/antarctic-geoserver)

## Source

- Polar view: https://www.polarview.aq/antarctic
- LIMA: https://lima.usgs.gov/access.php
- REMA: https://www.pgc.umn.edu/data/rema/
- BAS: https://www.bas.ac.uk/
